jQuery.noConflict();
jQuery(document).ready(function($){


	$('html').addClass('js');

	$('li').has('>ul').addClass('has-ul');

	$('.box_search .toggle').on('click',function(){
		$('body').toggleClass('box_search-on');
	})

	$('.menu_main .toggle').on('click',function(){
		$('body').toggleClass('menu_main-on');
	})


	// placeholder
	if (document.createElement('input').placeholder==undefined){
		$('[placeholder]').focus(function(){
			var input=$(this);
			if(input.val()==input.attr('placeholder')){
				input.val('');
				input.removeClass('placeholder')
			}
		}).blur(function(){
			var input=$(this);
			if(input.val()==''||input.val()==input.attr('placeholder')){
				input.addClass('placeholder');
				input.val(input.attr('placeholder'))
			}
		}).blur()
	}

});